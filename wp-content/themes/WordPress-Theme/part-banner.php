<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'nosotros' ) ) ) : dynamic_sidebar( 'banner_nosotros' ); endif; ?>
				<?php if ( is_page( array( 'capacidad' ) ) ) : dynamic_sidebar( 'banner_capacidad' ); endif; ?>
				<?php if ( is_page( array( 'conocimiento' ) ) ) : dynamic_sidebar( 'banner_conocimiento' ); endif; ?>
				<?php if ( is_page( array( 'experiencia' ) ) ) : dynamic_sidebar( 'banner_experiencia' ); endif; ?>
				<?php if ( is_page( array( 'repuestos' ) ) ) : dynamic_sidebar( 'banner_repuestos' ); endif; ?>
				<?php if ( is_page( array( 'servicio-tecnico-y-mantenimiento' ) ) ) : dynamic_sidebar( 'banner_servicio_tecnico' ); endif; ?>
				<?php if ( is_page( array( 'inspeccion-y-certificacion-de-puentes-grua' ) ) ) : dynamic_sidebar( 'banner_inspeccion_y_certificacion' ); endif; ?>
				<?php if ( is_page( array( 'asesoria-y-consultoria' ) ) ) : dynamic_sidebar( 'banner_asesoria_y_consultoria' ); endif; ?>
				<?php if ( is_page( array( 'postventa' ) ) ) : dynamic_sidebar( 'banner_postventa' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'banner_contactenos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
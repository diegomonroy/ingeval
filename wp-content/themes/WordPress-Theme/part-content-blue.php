<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content blue" data-wow-delay="0.5s">
		<div class="row align-center">
			<div class="small-12 medium-10 columns">
				<div class="container">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<h1 class="text-center"><?php the_title(); ?></h1>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->